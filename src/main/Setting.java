package main;

import javafx.beans.property.*;

public class Setting
{
    private final BooleanProperty sound = new SimpleBooleanProperty(this, "sound", false);
    private final BooleanProperty sfx = new SimpleBooleanProperty(this, "sfx", false);
    private final DoubleProperty volume = new SimpleDoubleProperty(this, "volume", 1.0);
    private final StringProperty username = new SimpleStringProperty(this, "username", "Player");
    private final IntegerProperty avatarNumber = new SimpleIntegerProperty(this, "avatarNumber", 1);

    public final BooleanProperty soundProperty() { return sound; }

    public final boolean getSfx() { return sfx.get(); }
    public final void setSfx(boolean value) { sfx.set(value); }
    public final BooleanProperty sfxProperty() { return sfx; }

    public final void setVolume(Double value) { volume.set(value); }
    public final DoubleProperty volumeProperty() { return  volume; }

    public final String getUsername() { return username.get(); }
    public final void setUsername(String value) { username.set(value); }

    public final int getAvatarNumber() { return avatarNumber.get(); }
    public final void setAvatarNumber(int value) { avatarNumber.set(value); }
}
