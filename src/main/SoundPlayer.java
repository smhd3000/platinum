package main;

import javafx.animation.Animation;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import static main.Main.setting;

public class SoundPlayer
{
    // a class for using music and audioClips in the game
    private boolean isMenu;

    private final MediaPlayer mediaPlayer1;
    private final MediaPlayer mediaPlayer2;

    private final AudioClip buttonClickSound;
    private final AudioClip buttonHoverSound;
    private final AudioClip toggleButtonSound;
    private final AudioClip connectToServerSound;
    private final AudioClip selectSound;

    public SoundPlayer()
    {
        Media media1 = new Media(getClass().getResource("resources/sounds/menu.mp3").toExternalForm());
        Media media2 = new Media(getClass().getResource("resources/sounds/game.mp3").toExternalForm());
        mediaPlayer1 = new MediaPlayer(media1);
        mediaPlayer1.setCycleCount(Animation.INDEFINITE);

        mediaPlayer2 = new MediaPlayer(media2);
        mediaPlayer2.setCycleCount(Animation.INDEFINITE);

        buttonClickSound = new AudioClip(getClass().getResource("resources/sounds/button_clicked.wav").toExternalForm());
        buttonHoverSound = new AudioClip(getClass().getResource("resources/sounds/button_hovered.wav").toExternalForm());
        toggleButtonSound = new AudioClip(getClass().getResource("resources/sounds/toggle_button.wav").toExternalForm());
        connectToServerSound = new AudioClip(getClass().getResource("resources/sounds/connect_to_server.wav").toExternalForm());
        selectSound = new AudioClip(getClass().getResource("resources/sounds/select.wav").toExternalForm());

        isMenu = true;
    }

    public void play()
    {
        if(isMenu)
            mediaPlayer1.play();
        else
            mediaPlayer2.play();
    }

    public void nextSound()
    {
        this.stop();

        if(isMenu)
            mediaPlayer2.play();
        else
            mediaPlayer1.play();

        isMenu = !isMenu;
    }

    public void setVolume(Double value)
    {
        mediaPlayer1.setVolume(value);
        mediaPlayer2.setVolume(value);
    }

    public void stop()
    {
        mediaPlayer1.stop();
        mediaPlayer2.stop();
    }

    public void mute()
    {
        mediaPlayer1.setMute(true);
        mediaPlayer2.setMute(true);
    }

    public void unMute()
    {
        mediaPlayer1.setMute(false);
        mediaPlayer2.setMute(false);
    }

    public void playButtonClick() { if(setting.getSfx()) buttonClickSound.play(); }
    public void playButtonHover() { if(setting.getSfx()) buttonHoverSound.play(); }
    public void playToggleClick() { if(setting.getSfx()) toggleButtonSound.play(); }
    public void playConnectToServer() { if(setting.getSfx()) connectToServerSound.play(); }
    public void playSelect() { if(setting.getSfx()) selectSound.play();}
}
