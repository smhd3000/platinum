package main.logic.games;

public interface TicTacToeConstants
{
    public static final int PORT = 8015;
    public static final String HOST = "localhost";

    public static final int PLAYER1 = 1;
    public static final int PLAYER2 = 2;
    public static final int DRAW = 3;
    public static final int CONTINUE = 4;
}
