package main.logic.games;

import com.jfoenix.controls.JFXSpinner;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import main.Main;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class TicTacToeOfflineController
{
    private char playerToken;
    private char AIToken;

    private char turn = 'X';
    private final char[][] board = new char[][] {{' ', ' ', ' '},
                                                 {' ', ' ', ' '},
                                                 {' ', ' ', ' '}};

    @FXML private Label instructionLabel;
    @FXML private GridPane gridPane;
    @FXML private Label serverLabel;
    @FXML private JFXSpinner serverSpinner;

    public void initFields(char playerToken)
    {
        this.playerToken = playerToken;
        if(playerToken == 'X')
            AIToken = 'O';
        else
            AIToken = 'X';

        refreshInstruction();
        if(turn == AIToken)
            move();
    }

    private void refreshInstruction()
    {
        Platform.runLater(() ->
        {
            if(turn == AIToken)
            {
                instructionLabel.setText("AI turn (" + AIToken + ")");
            } else
                instructionLabel.setText("Your turn (" + playerToken + ")");
        });
    }

    private boolean notMovesLeft(char[][] board)
    {
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                if (board[i][j] == ' ')
                    return false;
        return true;
    }

    private Node getNodeFromGridPane(GridPane gridPane, int col, int row)
    {
        for (Node node : gridPane.getChildren())
        {
            if (GridPane.getColumnIndex(node) == col && GridPane.getRowIndex(node) == row)
            {
                return node;
            }
        }
        return null;
    }

    private int evaluate(char[][] b)
    {
        // Checking for Rows for X or O victory.
        for (int row = 0; row < 3; row++)
        {
            if (b[row][0] == b[row][1] &&
                    b[row][1] == b[row][2])
            {
                if (b[row][0] == AIToken)
                    return +10;
                else if (b[row][0] == playerToken)
                    return -10;
            }
        }

        // Checking for Columns for X or O victory.
        for (int col = 0; col < 3; col++)
        {
            if (b[0][col] == b[1][col] &&
                    b[1][col] == b[2][col])
            {
                if (b[0][col] == AIToken)
                    return +10;

                else if (b[0][col] == playerToken)
                    return -10;
            }
        }

        // Checking for Diagonals for X or O victory.
        if (b[0][0] == b[1][1] && b[1][1] == b[2][2])
        {
            if (b[0][0] == AIToken)
                return +10;
            else if (b[0][0] == playerToken)
                return -10;
        }

        if (b[0][2] == b[1][1] && b[1][1] == b[2][0])
        {
            if (b[0][2] == AIToken)
                return +10;
            else if (b[0][2] == playerToken)
                return -10;
        }

        // Else if none of them have won then return 0
        return 0;
    }

    private int minimax(char[][] board, int depth, Boolean isMax)
    {
        int score = evaluate(board);

        // If Maximizer has won the game
        // return his/her evaluated score
        if (score == 10)
            return score;

        // If Minimizer has won the game
        // return his/her evaluated score
        if (score == -10)
            return score;

        // If there are no more moves and
        // no winner then it is a tie
        if (notMovesLeft(board))
            return 0;

        // If this maximizer's move
        int best;
        if (isMax)
        {
            best = -1000;

            // Traverse all cells
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    // Check if cell is empty
                    if (board[i][j] == ' ')
                    {
                        // Make the move
                        board[i][j] = AIToken;

                        // Call minimax recursively and choose
                        // the maximum value
                        best = Math.max(best, minimax(board, depth + 1, !isMax));

                        // Undo the move
                        board[i][j] = ' ';
                    }
                }
            }
        }

        // If this minimizer's move
        else
        {
            best = 1000;

            // Traverse all cells
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    // Check if cell is empty
                    if (board[i][j] == ' ')
                    {
                        // Make the move
                        board[i][j] = playerToken;

                        // Call minimax recursively and choose
                        // the minimum value
                        best = Math.min(best, minimax(board, depth + 1, !isMax));

                        // Undo the move
                        board[i][j] = ' ';
                    }
                }
            }
        }
        return best;
    }

    // This will return the best possible
// move for the player
    private int[] findBestMove(char[][] board)
    {
        int bestVal = -1000;
        int row, col;
        row = -1;
        col = -1;

        // Traverse all cells, evaluate minimax function
        // for all empty cells. And return the cell
        // with optimal value.
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                // Check if cell is empty
                if (board[i][j] == ' ')
                {
                    // Make the move
                    board[i][j] = AIToken;

                    // compute evaluation function for this
                    // move.
                    int moveVal = minimax(board, 0, false);

                    // Undo the move
                    board[i][j] = ' ';

                    // If the value of the current move is
                    // more than the best value, then update
                    // best/
                    if (moveVal > bestVal)
                    {
                        row = i;
                        col = j;
                        bestVal = moveVal;
                    }
                }
            }
        }

        return new int[]{row, col};
    }

    private void nextTurn()
    {
        refreshInstruction();
        if(turn == 'X')
            turn = 'O';
        else
            turn = 'X';

        if(turn == AIToken)
            move();
    }

    private void move()
    {
        int[] bestMove = findBestMove(board);
        int row = bestMove[0];
        int column = bestMove[1];

        Label label = (Label)getNodeFromGridPane(gridPane, column, row);
        assert label != null;
        new Thread(() ->
        {
            try { Thread.sleep(1500); } catch (InterruptedException exception) { exception.printStackTrace(); }

            Platform.runLater(() -> label.setText(String.valueOf(AIToken)));
            board[row][column] = AIToken;
            Platform.runLater(() -> label.setDisable(true));
            checkGameIsFinished();
        }).start();
    }

    private void checkGameIsFinished()
    {
        if (evaluate(board) == 10)
        {
            Platform.runLater(() -> instructionLabel.setText("AI won! (" + AIToken + ")"));
            for(Node node : gridPane.getChildren())
                node.setDisable(true);
            sendDataToServer(1);
        }
        else if(evaluate(board) == -10)
        {
            Platform.runLater(() -> instructionLabel.setText("You won! (" + playerToken + ")"));
            for(Node node : gridPane.getChildren())
                node.setDisable(true);
            sendDataToServer(0);
        }
        else if(notMovesLeft(board))
        {
            Platform.runLater(() -> instructionLabel.setText("It's a draw!"));
            sendDataToServer(2);
        }
        else
            nextTurn();
    }

    private void sendDataToServer(int condition)
    {
        // 0 player won
        // 1 AI won
        // 2 draw

        int[] data = new int[]{0, 0, 0, 0, 0, 0};
        switch (condition)
        {
            case 0:
                data[0] = 1;
                break;
            case 1:
                data[2] = 1;
                break;
            case 2:
                data[1] = 1;
                break;
        }
        new Thread(() ->
        {
            try
            {
                Platform.runLater(() ->
                {
                    serverLabel.setVisible(true);
                    serverSpinner.setVisible(true);
                });
                Thread.sleep(1000);

                Socket socket = new Socket("localhost", Main.UPDATE_GAME_STATISTICS);
                Platform.runLater(() -> serverLabel.setText("Connected to server"));
                Thread.sleep(2000);

                Platform.runLater(() -> serverLabel.setText("sending information to database"));

                DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());
                toServer.writeUTF(Main.setting.getUsername());
                toServer.writeUTF("TicTacToe");
                toServer.writeInt(6);
                for(int i = 0; i < 6; i++)
                {
                    toServer.writeInt(data[i]);
                }
                Thread.sleep(1000);

                Platform.runLater(() -> serverLabel.setText("Done!"));
                Thread.sleep(1000);
                Main.loadFXML("ui/home/Home.fxml");
            } catch (IOException | InterruptedException e)
            {
                e.printStackTrace();
            }
        }).start();


    }

    @FXML
    private void cellClicked(MouseEvent event)
    {
        if(turn == playerToken)
        {
            Label label = (Label) event.getSource();
            label.setText(String.valueOf(playerToken));
            label.setDisable(true);
            int column = GridPane.getColumnIndex(label);
            int row = GridPane.getRowIndex(label);

            board[row][column] = playerToken;

            checkGameIsFinished();
        }
    }
}
