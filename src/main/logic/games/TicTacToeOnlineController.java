package main.logic.games;

import com.jfoenix.controls.JFXSpinner;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import main.Main;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;

public class TicTacToeOnlineController implements TicTacToeConstants
{
    @FXML private Label label00;
    @FXML private Label label01;
    @FXML private Label label02;
    @FXML private Label label10;
    @FXML private Label label11;
    @FXML private Label label12;
    @FXML private Label label20;
    @FXML private Label label21;
    @FXML private Label label22;

    @FXML private Label titleLabel;
    @FXML private Label instructionLabel;
    @FXML private Label serverLabel;
    @FXML private JFXSpinner serverSpinner;

    private DataInputStream fromServer;
    private DataOutputStream toServer;

    private int rowSelected;
    private int columnSelected;
    private int player;
    private char myToken;
    private char otherToken;
    private boolean myTurn = false;
    private boolean isWaiting = true;
    private boolean gameEnded = false;

    private Label[][] labels;

    public void initFields(int player, Socket socket)
    {
        labels =  new Label[][]{{label00, label01, label02},
                                {label10, label11, label12},
                                {label20, label21, label22}};
        this.player = player;

        new Thread(() ->
        {
            try
            {
                fromServer = new DataInputStream(socket.getInputStream());
                toServer = new DataOutputStream(socket.getOutputStream());

                if (player == PLAYER1)
                {
                    Platform.runLater(() -> titleLabel.setText("you are Player 1 (X)"));
                    Platform.runLater(() -> instructionLabel.setText("please choose a cell"));
                    myToken = 'X';
                    otherToken = 'O';
                    myTurn = true;
                } else
                {
                    myToken = 'O';
                    otherToken = 'X';
                    Platform.runLater(() -> titleLabel.setText("you are Player 2 (O)"));
                    Platform.runLater(() -> instructionLabel.setText("Wait for player 1"));
                }

                if (player == PLAYER1)
                {
                    while (!gameEnded)
                    {
                        sendInfo();
                        if (getStatus() == CONTINUE)
                        {
                            receiveInfo();
                            getStatus();
                        }
                    }

                } else
                {
                    while (!gameEnded)
                    {
                        receiveInfo();
                        if (getStatus() == CONTINUE)
                        {
                            sendInfo();
                            getStatus();
                        }
                    }
                }

            } catch (ConnectException exception)
            {
                Platform.runLater(() -> instructionLabel.setText("Cannot connect to server"));
                try
                {
                    Thread.sleep(3000);
                } catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
                System.exit(0);
            } catch (IOException exception)
            {
                exception.printStackTrace();
            }

        }).start();
    }

    private void sendInfo()
    {
        waitForUser();

        try
        {
            toServer.writeInt(rowSelected);
            toServer.writeInt(columnSelected);
        } catch (IOException exception)
        {
            exception.printStackTrace();
        }
    }

    private void waitForUser()
    {
        try
        {
            while(isWaiting)
            {
                Thread.sleep(100);
            }
        } catch (InterruptedException exception)
        {
            exception.printStackTrace();
        }
        isWaiting = true;
    }

    private int getStatus()
    {
        int status = 0;
        try
        {
            status = fromServer.readInt();
            switch (status)
            {
                case PLAYER1:
                    gameEnded = true;

                    if(player == PLAYER1)
                        Platform.runLater(() -> titleLabel.setText("You won!"));
                    else
                        Platform.runLater(() -> titleLabel.setText("You lost :("));

                    Platform.runLater(() -> instructionLabel.setText("Thanks for playing..."));
                    gameFinished(1);
                    break;

                case PLAYER2:
                    gameEnded = true;

                    if(player == PLAYER1)
                        Platform.runLater(() -> titleLabel.setText("You lost :("));
                    else
                        Platform.runLater(() -> titleLabel.setText("You won!"));

                    Platform.runLater(() -> instructionLabel.setText("Thanks for playing..."));
                    gameFinished(2);
                    break;

                case DRAW:
                    gameEnded = true;

                    Platform.runLater(() -> titleLabel.setText("It is a draw :)"));
                    Platform.runLater(() -> instructionLabel.setText("Thanks for playing..."));
                    gameFinished(0);
                    break;

                case CONTINUE:
                    myTurn = !myTurn;
                    if(myTurn)
                        Platform.runLater(() -> instructionLabel.setText("Your turn"));
                    else
                        Platform.runLater(() -> instructionLabel.setText("waiting for other player..."));
                    break;
            }
        } catch (IOException exception)
        {
            exception.printStackTrace();
        }
        return status;
    }

    private void receiveInfo()
    {
        try
        {
            rowSelected = fromServer.readInt();
            columnSelected = fromServer.readInt();

            Platform.runLater(() ->
            {
                labels[rowSelected][columnSelected].setText(String.valueOf(otherToken));
                labels[rowSelected][columnSelected].setDisable(true);
            });
        } catch (IOException exception)
        {
            exception.printStackTrace();
        }
    }

    @FXML
    private void cellClicked(MouseEvent event)
    {
        Label label = (Label) event.getSource();
        if(myTurn && !gameEnded)
        {
            Platform.runLater(() ->
            {
                label.setText(String.valueOf(myToken));
                label.setDisable(true);
            });
            rowSelected = Integer.parseInt(label.getId().substring(0, 1));
            columnSelected = Integer.parseInt(label.getId().substring(1, 2));
            isWaiting = false;
        }
    }

    private void gameFinished(int status)
    {
        int[] data = new int[]{0, 0, 0, 0, 0, 0};

        switch (status)
        {
            case 0:
                data[4] = 1;
                break;
            case 1:
                if(player == PLAYER1)
                    data[3] = 1;
                else
                    data[5] = 1;
                break;
            case 2:
                if(player == PLAYER2)
                    data[3] = 1;
                else
                    data[5] = 1;
                break;
        }

        new Thread(() ->
        {
            try
            {
                Platform.runLater(() ->
                {
                    serverLabel.setVisible(true);
                    serverSpinner.setVisible(true);
                });
                Thread.sleep(1000);

                Socket socket = new Socket("localhost", Main.UPDATE_GAME_STATISTICS);
                Platform.runLater(() -> serverLabel.setText("Connected to server"));
                Thread.sleep(2000);

                Platform.runLater(() -> serverLabel.setText("sending information to database"));

                DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());
                toServer.writeUTF(Main.setting.getUsername());
                toServer.writeUTF("TicTacToe");
                toServer.writeInt(6);
                for(int i = 0; i < 6; i++)
                {
                    toServer.writeInt(data[i]);
                }
                Thread.sleep(1000);

                Platform.runLater(() -> serverLabel.setText("Done!"));

                Thread.sleep(1000);
                Main.loadFXML("ui/home/Home.fxml");
            } catch (IOException | InterruptedException e)
            {
                e.printStackTrace();
            }
        }).start();
    }
}
