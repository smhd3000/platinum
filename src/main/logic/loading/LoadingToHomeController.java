package main.logic.loading;

import animatefx.animation.JackInTheBox;
import com.jfoenix.controls.JFXProgressBar;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.util.Duration;
import main.Main;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoadingToHomeController implements Initializable
{
    @FXML private JFXProgressBar progressBar;
    @FXML private FontAwesomeIconView shuttle;
    @FXML private Label description;
    @FXML private Label loading;

    private Parent home;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        loadHome();
        handleDescription();

        // animate components
        new JackInTheBox(loading).setSpeed(0.5).play();
        new JackInTheBox(description).setSpeed(0.5).play();
        new JackInTheBox(shuttle).setSpeed(0.5).play();
        new JackInTheBox(progressBar).setSpeed(0.5).play();

        // play music
        Main.mediaPlayer.play();

        // move progress bar
        progressBar.setProgress(0);
        Timeline timeline = new Timeline();
        KeyValue keyValue = new KeyValue(progressBar.progressProperty(), 1, Interpolator.EASE_BOTH);
        KeyFrame keyFrame = new KeyFrame(Duration.seconds(10), keyValue);
        timeline.getKeyFrames().add(keyFrame);
        timeline.play();

        // move shuttle
        Timeline timeline1 = new Timeline();
        KeyValue keyValue1 = new KeyValue(shuttle.translateXProperty(), progressBar.getPrefWidth(), Interpolator.EASE_BOTH);
        KeyFrame keyFrame1 = new KeyFrame(Duration.seconds(10), keyValue1);
        timeline1.getKeyFrames().add(keyFrame1);
        timeline1.play();

        // go to home page
        timeline1.setOnFinished(event -> Main.loadFXML(home));
    }

    private void loadHome()
    {
        new Thread(() ->
        {
            try
            {
                home = FXMLLoader.load(getClass().getResource("../../ui/home/Home.fxml"));
            } catch (IOException exception)
            {
                exception.printStackTrace();
            }
        }).start();
    }

    private void handleDescription()
    {
        new Thread(() ->
        {
            try
            {
                Platform.runLater(() -> description.setText("Connecting to server"));
                for(int i = 0; i < 5; i++)
                {
                    Thread.sleep(500);
                    Platform.runLater(() -> description.setText(description.getText() + "."));
                }
                Thread.sleep(500);

                Platform.runLater(() -> description.setText("Fetching data"));
                for(int i = 0; i < 4; i++)
                {
                    Thread.sleep(600);
                    Platform.runLater(() -> description.setText(description.getText() + "."));
                }
                Thread.sleep(600);

                Platform.runLater(() -> description.setText("Building components"));
                Thread.sleep(3000);
                Platform.runLater(() -> description.setText("Loading completed!"));
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }).start();
    }
}
