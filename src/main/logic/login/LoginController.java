package main.logic.login;

import animatefx.animation.FadeIn;
import animatefx.animation.RubberBand;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXSpinner;
import com.jfoenix.controls.JFXTextField;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import main.Main;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable
{
    @FXML private JFXTextField usernameTF;
    @FXML private JFXPasswordField passwordTF;
    @FXML private JFXButton loginButton;
    @FXML private JFXButton signUpButton;
    @FXML private Label serverLabel;
    @FXML private JFXSpinner serverSpinner;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        bindComponents();
    }

    private void bindComponents()
    {
        usernameTF.textProperty().addListener((observable, oldValue, newValue) ->
        {
            setButtonDisable();
            if(newValue.length() < 4 || !newValue.substring(0, 1).matches("[A-Za-z_]"))
            {
                usernameTF.setStyle("-jfx-focus-color: red");
            }
            else
            {
                usernameTF.setStyle("-fx-focus-color: #7f15b9");
            }
        });

        passwordTF.textProperty().addListener((observable, oldValue, newValue) ->
        {
            setButtonDisable();
            if(newValue.length() < 8)
            {
                passwordTF.setStyle("-jfx-focus-color: red");
            }
            else
            {
                passwordTF.setStyle("-fx-focus-color: #7f15b9");
            }
        });
    }

    private void setButtonDisable()
    {
        if(usernameTF.getText().length() >= 4 && usernameTF.getText().substring(0, 1).matches("[A-Za-z_]"))
            if(passwordTF.getText().length() >= 8)
            {
                loginButton.setDisable(false);
                return;
            }

        loginButton.setDisable(true);
    }



    @FXML
    private void signUpButtonPressed()
    {
        Main.mediaPlayer.playButtonClick();
        RubberBand signUpButtonAnimation = new RubberBand(signUpButton);
        signUpButtonAnimation.play();
        signUpButtonAnimation.setOnFinished(event -> Main.loadFXML("ui/login/SignUp.fxml"));
    }

    @FXML
    private void forgetHLPressed()
    {
        Main.loadFXML("ui/login/RestorePassword.fxml");
    }

    @FXML
    private void loginButtonPressed()
    {
        Main.mediaPlayer.playButtonClick();
        Main.mediaPlayer.playConnectToServer();
        new RubberBand(loginButton).play();

        serverLabel.setVisible(true);
        serverSpinner.setVisible(true);
        new FadeIn(serverLabel).setSpeed(0.5).play();
        new FadeIn(serverSpinner).setSpeed(0.5).play();

        // connect to server for data validation
        new Thread(() ->
        {
            try
            {
                Socket socket = new Socket("localhost", Main.LOGIN_PORT);
                Thread.sleep(1000);
                Platform.runLater(() -> serverLabel.setText("Connected to server!"));
                Thread.sleep(500);

                DataInputStream fromServer = new DataInputStream(socket.getInputStream());
                DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());

                toServer.writeUTF(usernameTF.getText());
                toServer.writeUTF(passwordTF.getText());
                boolean successful = fromServer.readBoolean();

                if (successful)
                {
                    // update settings
                    Main.setting.setUsername(usernameTF.getText());
                    Main.getAvatarNumber();

                    // go to home page
                    Platform.runLater(() -> serverLabel.setText("Welcome!"));
                    Thread.sleep(1500);
                    Main.loadFXML("ui/loading/LoadingToHome.fxml");
                } else
                {
                    // react to user for mismatch
                    Platform.runLater(() -> serverLabel.setText("username or password is incorrect!"));
                    Thread.sleep(1500);

                    // reset the server label and spinner
                    Platform.runLater(() ->
                    {
                        serverLabel.setVisible(false);
                        serverSpinner.setVisible(false);
                        serverLabel.setText("connecting to server...");
                    });
                }

            } catch (ConnectException connectException)
            {
                // react to user for connection problem
                Platform.runLater(() -> serverLabel.setText("couldn't connect to server!"));
                try
                {
                    Thread.sleep(1500);
                } catch (InterruptedException exception)
                {
                    exception.printStackTrace();
                }

                // reset the server label and spinner
                Platform.runLater(() ->
                {
                    serverLabel.setVisible(false);
                    serverSpinner.setVisible(false);
                    serverLabel.setText("connecting to server...");
                });
            } catch (IOException | InterruptedException exception)
            {
                exception.printStackTrace();
            }
        }).start();
    }
}
