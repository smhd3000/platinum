package main.logic.login;

import animatefx.animation.FadeIn;
import animatefx.animation.RubberBand;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXSpinner;
import com.jfoenix.controls.JFXTextField;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import main.Main;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;

public class RestorePasswordController implements Initializable
{
    @FXML private JFXTextField username;
    @FXML private JFXComboBox<String> question;
    @FXML private JFXTextField answer;
    @FXML private JFXButton restore;
    @FXML private JFXSpinner serverSpinner;
    @FXML private Label serverLabel;
    @FXML private JFXTextField passwordTF;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        // add data to comboBox
        question.getItems().add("What is your favorite food?");
        question.getItems().add("Who is your best friend?");
        question.getItems().add("Where was your first home?");
        question.getItems().add("What is your special ability?");

        bindComponents();
    }

    private void bindComponents()
    {
        username.textProperty().addListener((observable, oldValue, newValue) ->
        {
            setButtonDisable();
            if(newValue.length() < 4 || !newValue.substring(0, 1).matches("[A-Za-z_]"))
            {
                username.setStyle("-jfx-focus-color: red");
            }
            else
            {
                username.setStyle("-fx-focus-color: #7f15b9");
            }
        });
        question.valueProperty().addListener((observable, oldValue, newValue) -> setButtonDisable());
        answer.textProperty().addListener((observable, oldValue, newValue) -> setButtonDisable());
    }

    private void setButtonDisable()
    {
        if (username.getText().length() >= 4 && username.getText().substring(0, 1).matches("[A-Za-z_]"))
            if (!question.getSelectionModel().isEmpty())
                if (!answer.getText().isEmpty())
                {
                    restore.setDisable(false);
                    return;
                }
        restore.setDisable(true);
    }

    @FXML
    private void backPressed()
    {
        Main.loadFXML("ui/login/Login.fxml");
    }

    @FXML
    private void restorePressed()
    {
        Main.mediaPlayer.playButtonClick();
        Main.mediaPlayer.playConnectToServer();
        new RubberBand(restore).play();

        serverLabel.setVisible(true);
        serverSpinner.setVisible(true);
        new FadeIn(serverLabel).setSpeed(0.5).play();
        new FadeIn(serverSpinner).setSpeed(0.5).play();

        // connect to server to check input validation
        new Thread(() ->
        {
            try
            {
                // check weather this username or email is valid
                Socket socket = new Socket("localhost", Main.RESTORE_PORT);
                Thread.sleep(1000);
                Platform.runLater(() -> serverLabel.setText("Connected to server!"));
                Thread.sleep(500);

                DataInputStream fromServer = new DataInputStream(socket.getInputStream());
                DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());

                toServer.writeUTF(username.getText());
                toServer.writeUTF(question.getSelectionModel().getSelectedItem());
                toServer.writeUTF(answer.getText());

                boolean successful = fromServer.readBoolean();
                if(successful)
                {
                    Platform.runLater(() ->
                    {
                        try
                        {
                            passwordTF.setText(fromServer.readUTF());
                            serverLabel.setText("here you are!");
                            Thread.sleep(1500);
                            serverLabel.setVisible(false);
                            serverSpinner.setVisible(false);
                            serverLabel.setText("connecting to server...");
                        } catch (IOException | InterruptedException exception)
                        {
                            exception.printStackTrace();
                        }
                    });
                } else
                {
                    Platform.runLater(() -> serverLabel.setText("something is wrong!"));
                    Thread.sleep(1500);
                    Platform.runLater(() ->
                    {
                        serverLabel.setVisible(false);
                        serverSpinner.setVisible(false);
                        serverLabel.setText("connecting to server...");
                    });
                }
            } catch (ConnectException connectException)
            {
                // react to user for connection problem
                Platform.runLater(() -> serverLabel.setText("couldn't connect to server!"));
                try
                {
                    Thread.sleep(1500);
                } catch (InterruptedException exception)
                {
                    exception.printStackTrace();
                }

                // reset the server label and spinner
                Platform.runLater(() ->
                {
                    serverLabel.setVisible(false);
                    serverSpinner.setVisible(false);
                    serverLabel.setText("connecting to server...");
                });
            } catch (IOException | InterruptedException exception)
            {
                exception.printStackTrace();
            }
        }).start();
    }
}
