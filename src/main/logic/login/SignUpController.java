package main.logic.login;

import animatefx.animation.FadeIn;
import animatefx.animation.RubberBand;
import com.jfoenix.controls.*;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import main.Main;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;

public class SignUpController implements Initializable
{
    @FXML private JFXTextField username;
    @FXML private JFXPasswordField password;
    @FXML private JFXPasswordField password2;
    @FXML private JFXTextField email;
    @FXML private JFXTextField answer;
    @FXML private JFXComboBox<String> question;
    @FXML private JFXButton submit;
    @FXML private JFXToggleButton terms;
    @FXML private JFXSpinner serverSpinner;
    @FXML private Label serverLabel;

    private static String regex;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        regex = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0" +
                "e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a" +
                "-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]" +
                "?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-" +
                "\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)])";

        // add data to comboBox
        question.getItems().add("What is your favorite food?");
        question.getItems().add("Who is your best friend?");
        question.getItems().add("Where was your first home?");
        question.getItems().add("What is your special ability?");

        bindComponents();
    }

    private void bindComponents()
    {
        username.textProperty().addListener((observable, oldValue, newValue) ->
        {
            setButtonDisable();
            if(newValue.length() < 4 || !newValue.substring(0, 1).matches("[A-Za-z_]") || newValue.contains(" "))
            {
                username.setStyle("-jfx-focus-color: red");
            }
            else
            {
                username.setStyle("-fx-focus-color: #7f15b9");
            }
        });

        email.textProperty().addListener((observable, oldValue, newValue) ->
        {
            setButtonDisable();
            if(!newValue.matches(regex))
            {
                email.setStyle("-jfx-focus-color: red");
            }
            else
            {
                email.setStyle("-fx-focus-color: #7f15b9");
            }
        });

        password.textProperty().addListener((observable, oldValue, newValue) ->
        {
            setButtonDisable();
            if(newValue.length() < 8)
            {
                password.setStyle("-jfx-focus-color: red");
            }
            else
            {
                password.setStyle("-fx-focus-color: #7f15b9");
            }
        });

        password2.textProperty().addListener((observable, oldValue, newValue) ->
        {
            setButtonDisable();
            if(!newValue.equals(password.getText()))
            {
                password2.setStyle("-jfx-focus-color: red");
            }
            else
            {
                password2.setStyle("-fx-focus-color: #7f15b9");
            }
        });

        question.valueProperty().addListener((observable, oldValue, newValue) -> setButtonDisable());
        answer.textProperty().addListener((observable, oldValue, newValue) -> setButtonDisable());
        terms.selectedProperty().addListener((observable, oldValue, newValue) ->
        {
            Main.mediaPlayer.playToggleClick();
            setButtonDisable();
        });
    }

    private void setButtonDisable()
    {
        if (username.getText().length() >= 4 && username.getText().substring(0, 1).matches("[A-Za-z_]") && !username.getText().contains(" "))
            if (email.getText().matches(regex))
                if (password.getText().length() >= 8)
                    if (password2.getText().equals(password.getText()))
                        if (!question.getSelectionModel().isEmpty())
                            if (!answer.getText().isEmpty())
                                if(terms.isSelected())
                                {
                                    submit.setDisable(false);
                                    return;
                                }
        submit.setDisable(true);
    }

    @FXML
    private void backPressed()
    {
        Main.loadFXML("ui/login/Login.fxml");
    }

    @FXML
    private void submitPressed()
    {
        Main.mediaPlayer.playButtonClick();
        Main.mediaPlayer.playConnectToServer();
        new RubberBand(submit).play();

        serverLabel.setVisible(true);
        serverSpinner.setVisible(true);
        new FadeIn(serverLabel).setSpeed(0.5).play();
        new FadeIn(serverSpinner).setSpeed(0.5).play();

        new Thread(() ->
        {
            try
            {
                // check weather this username or email is already taken
                Socket socket = new Socket("localhost", Main.SIGN_UP_PORT);
                Thread.sleep(1000);
                Platform.runLater(() -> serverLabel.setText("Connected to server!"));
                Thread.sleep(500);

                DataInputStream fromServer = new DataInputStream(socket.getInputStream());
                DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());

                toServer.writeUTF(username.getText());
                toServer.writeUTF(password.getText());
                toServer.writeUTF(email.getText());
                toServer.writeUTF(question.getSelectionModel().getSelectedItem());
                toServer.writeUTF(answer.getText());

                boolean successful = fromServer.readBoolean();
                if(successful)
                {
                    // update settings
                    Main.setting.setUsername(username.getText());
                    Main.getAvatarNumber();

                    // go to home page
                    Platform.runLater(() -> serverLabel.setText("Welcome!"));
                    Thread.sleep(1500);
                    Main.loadFXML("ui/loading/LoadingToHome.fxml");
                } else
                {
                    Platform.runLater(() -> serverLabel.setText("username or email is duplicate!"));
                    Thread.sleep(1500);
                    Platform.runLater(() ->
                    {
                        serverLabel.setVisible(false);
                        serverSpinner.setVisible(false);
                        serverLabel.setText("connecting to server...");
                    });
                }
            } catch (ConnectException connectException)
            {
                // react to user for connection problem
                Platform.runLater(() -> serverLabel.setText("couldn't connect to server!"));
                try
                {
                    Thread.sleep(1500);
                } catch (InterruptedException exception)
                {
                    exception.printStackTrace();
                }

                // reset the server label and spinner
                Platform.runLater(() ->
                {
                    serverLabel.setVisible(false);
                    serverSpinner.setVisible(false);
                    serverLabel.setText("connecting to server...");
                });
            } catch (IOException | InterruptedException exception)
            {
                exception.printStackTrace();
            }
        }).start();
    }
}
