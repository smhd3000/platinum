package main.logic.home;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import main.Main;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;

public class FriendHBoxController
{
    @FXML private FontAwesomeIconView trashButton;
    @FXML private FontAwesomeIconView sendButton;
    @FXML private Label usernameLabel;
    @FXML private Label lastSeenLabel;
    @FXML private Circle avatarCircle;
    @FXML private Circle onlineStatusCircle;

    private HomeController homeController;

    public void initFields(String username, String lastSeen, boolean isOnline, int avatarNumber, HomeController homeController)
    {
        usernameLabel.setText(username);
        if(isOnline)
        {
            makeOnline();
        } else
        {
            makeOffline(lastSeen);
        }
        avatarCircle.setFill(new ImagePattern(new Image("main/resources/pictures/user" + avatarNumber + ".png")));
        this.homeController = homeController;
    }

    public void makeOnline()
    {
        Platform.runLater(() ->
        {
            lastSeenLabel.setText("");
            onlineStatusCircle.setStyle("-fx-fill: green");
        });
    }

    public void makeOffline(String lastSeen)
    {
        Platform.runLater(() ->
        {
            lastSeenLabel.setText(lastSeen);
            onlineStatusCircle.setStyle("-fx-fill: red");
        });
    }

    @FXML
    void deleteClicked()
    {
        try
        {
            // delete friend from server
            Socket socket = new Socket("localhost", Main.DELETE_FRIEND);
            DataInputStream fromServer = new DataInputStream(socket.getInputStream());
            DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());

            toServer.writeUTF(Main.setting.getUsername());
            toServer.writeUTF(usernameLabel.getText());

            if (fromServer.readBoolean())
            {
                // update vBox to disappear this hBox
                new Thread(() ->
                {
                    try
                    {
                        Platform.runLater(() -> trashButton.setFill(Color.GREEN));
                        Thread.sleep(1000);
                        homeController.updateFriendVBox();
                        homeController.updateChatNameVBox();
                    } catch (InterruptedException exception)
                    {
                        exception.printStackTrace();
                    }
                }).start();
            } else
            {
                // make visual effect to show something is wrong
                makeTrashButtonRed();
            }
        } catch (ConnectException e)
        {
            makeTrashButtonRed();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private void makeTrashButtonRed()
    {
        new Thread(() ->
        {
            try
            {
                Platform.runLater(() -> trashButton.setFill(Color.RED));
                Thread.sleep(2000);
                Platform.runLater(() -> trashButton.setFill(Color.WHITE));
            } catch (InterruptedException exception)
            {
                exception.printStackTrace();
            }
        }).start();
    }

    @FXML
    void sendMessageClicked(MouseEvent event)
    {
        // goto the username chatroom
    }
}
