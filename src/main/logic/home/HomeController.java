package main.logic.home;

import animatefx.animation.FadeIn;
import animatefx.animation.RubberBand;
import animatefx.animation.SlideInUp;
import animatefx.animation.ZoomIn;
import com.jfoenix.controls.*;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import main.Main;
import main.logic.games.TicTacToeConstants;
import main.logic.games.TicTacToeOfflineController;
import main.logic.games.TicTacToeOnlineController;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.net.SocketException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class HomeController implements Initializable
{
    // home
    @FXML private Circle avatarCircle;
    @FXML private Label avatarLabel;
    @FXML public Rectangle feedRectangle;
    @FXML private Label offlineGamesPlayed;
    @FXML private Label offlineGamesWon;
    @FXML private Label offlineGamesDrawn;
    @FXML private Label offlineGamesLost;
    @FXML private Label onlineGamesPlayed;
    @FXML private Label onlineGamesWon;
    @FXML private Label onlineGamesDrawn;
    @FXML private Label onlineGamesLost;

    // game
    @FXML private JFXRadioButton xRadioButton;
    @FXML private JFXComboBox<String> gameComboBox;

    // chat
    @FXML private VBox chatNameVBox;
    @FXML private VBox chatMessageVBox;
    @FXML public Label information;
    @FXML private TextArea chatTA;
    @FXML private ScrollPane chatScrollPane;

    // friend
    @FXML private JFXTextField addFriendTF;
    @FXML private JFXSpinner friendServerSpinner;
    @FXML private Label friendServerLabel;
    @FXML private VBox friendsVBox;

    // setting
    @FXML private JFXToggleButton musicToggle;
    @FXML private JFXToggleButton sfxToggle;
    @FXML private JFXSlider volumeSlider;
    @FXML private JFXButton exitButton;
    @FXML private JFXButton logOutButton;

    @FXML private Circle avatarCircle1;
    @FXML private Circle avatarCircle2;
    @FXML private Circle avatarCircle3;
    @FXML private Circle avatarCircle4;
    @FXML private Circle avatarCircle5;
    @FXML private Circle avatarCircle6;
    @FXML private Circle avatarCircle7;
    @FXML private Circle avatarCircle8;
    @FXML private Circle avatarCircle9;
    @FXML private Circle avatarCircle10;
    @FXML private Circle avatarCircle11;
    @FXML private Circle avatarCircle12;

    private Circle[] circles;

    private final Map<String, FriendHBoxController> friendHBoxControllers = new HashMap<>();
    private final Map<String, ChatNameHBoxController> chatNameHBoxControllers = new HashMap<>();

    // fields
    private static boolean isInHome = true;
    public String chatSelected = "";
    private Socket gameSocket;
    private Socket onlineSocket;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        makeOnline();
        circles = new Circle[]{avatarCircle1, avatarCircle2, avatarCircle3, avatarCircle4, avatarCircle5,
                avatarCircle6, avatarCircle7, avatarCircle8, avatarCircle9, avatarCircle10, avatarCircle11, avatarCircle12};

        new Thread(new HomeHandler(feedRectangle)).start();

        new Thread(this::updateFriendVBox).start();
        new Thread(this::updateChatNameVBox).start();

        new Thread(new ChatHandler()).start();
        new Thread(new GameHandler()).start();

        new Thread(new SettingHandler()).start();
    }

    private void makeOnline()
    {
        new Thread(() ->
        {
            try
            {
                onlineSocket = new Socket("localhost", Main.ONLINE_PORT);
                DataOutputStream toServer = new DataOutputStream(onlineSocket.getOutputStream());
                DataInputStream fromServer = new DataInputStream(onlineSocket.getInputStream());

                toServer.writeUTF(Main.setting.getUsername());
                toServer.writeBoolean(true);

                while (isInHome)
                {
                    String friendName = fromServer.readUTF();
                    boolean isOnline = fromServer.readBoolean();

                    // something to be added

                    if (isOnline)
                    {
                        friendHBoxControllers.get(friendName).makeOnline();
                        chatNameHBoxControllers.get(friendName).makeOnline();
                    } else
                    {
                        friendHBoxControllers.get(friendName).makeOffline(new Date().toString());
                        chatNameHBoxControllers.get(friendName).makeOffline(new Date().toString());
                    }
                }
            } catch (SocketException ignored) {}
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }).start();
    }

    private Object[] getInfo()
    {
        int friendNumber = 0;
        String[] friendList = new String[0], lastSeenList = new String[0];
        boolean[] isOnlineList = new boolean[0];
        int[] avatarList = new int[0];

        try
        {
            Socket socket = new Socket("localhost", Main.GET_FRIEND_INFO);
            DataInputStream fromServer = new DataInputStream(socket.getInputStream());
            DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());

            toServer.writeUTF(Main.setting.getUsername());

            friendNumber = fromServer.readInt();

            friendList = new String[friendNumber];
            lastSeenList = new String[friendNumber];
            isOnlineList = new boolean[friendNumber];
            avatarList = new int[friendNumber];

            for(int i = 0; i < friendNumber; i++)
            {
                friendList[i] = fromServer.readUTF();
                lastSeenList[i] = fromServer.readUTF();
                isOnlineList[i] = fromServer.readBoolean();
                avatarList[i] = fromServer.readInt();
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return new Object[]{friendNumber, friendList, lastSeenList, isOnlineList, avatarList};
    }

    // home
    class HomeHandler implements Runnable
    {
        private final Rectangle feedRectangle;

        public HomeHandler(Rectangle feedRectangle)
        {
            this.feedRectangle = feedRectangle;
        }

        @Override
        public void run()
        {
            int avatarNumber = Main.setting.getAvatarNumber();
            avatarCircle.setFill(new ImagePattern(new Image("main/resources/pictures/user" + avatarNumber + ".png")));
            avatarLabel.setText(Main.setting.getUsername());

            int[] ticTacToeStatistics = new int[6];
            try
            {
                Socket socket = new Socket("localhost", Main.GAME_STATISTICS);
                DataInputStream fromSever = new DataInputStream(socket.getInputStream());
                DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());

                toServer.writeUTF(Main.setting.getUsername());
                toServer.writeUTF("TicTacToe");

                for(int i = 0; i < 6; i++)
                {
                    ticTacToeStatistics[i] = fromSever.readInt();
                }
            } catch (IOException e)
            {
                e.printStackTrace();
            }
            Platform.runLater(() ->
            {
                offlineGamesPlayed.setText(String.valueOf(ticTacToeStatistics[0] + ticTacToeStatistics[1] + ticTacToeStatistics[2]));
                offlineGamesWon.setText(String.valueOf(ticTacToeStatistics[0]));
                offlineGamesDrawn.setText(String.valueOf(ticTacToeStatistics[1]));
                offlineGamesLost.setText(String.valueOf(ticTacToeStatistics[2]));

                onlineGamesPlayed.setText(String.valueOf(ticTacToeStatistics[3] + ticTacToeStatistics[4] + ticTacToeStatistics[5]));
                onlineGamesWon.setText(String.valueOf(ticTacToeStatistics[3]));
                onlineGamesDrawn.setText(String.valueOf(ticTacToeStatistics[4]));
                onlineGamesLost.setText(String.valueOf(ticTacToeStatistics[5]));
            });

            ImagePattern feed1 = new ImagePattern(new Image("main/resources/pictures/feed1.jpg"));
            ImagePattern feed2 = new ImagePattern(new Image("main/resources/pictures/feed2.jpg"));
            ImagePattern feed3 = new ImagePattern(new Image("main/resources/pictures/feed3.jpg"));
            ImagePattern feed4 = new ImagePattern(new Image("main/resources/pictures/feed4.jpg"));
            ImagePattern[] feeds = new ImagePattern[]{feed1, feed2, feed3, feed4};

            try
            {
                while(isInHome)
                {
                    for (ImagePattern feed : feeds)
                    {
                        feedRectangle.setFill(feed);
                        new SlideInUp(feedRectangle).setSpeed(0.5).play();
                        Thread.sleep(5000);
                    }
                }
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }

    // game
    class GameHandler implements Runnable
    {
        @Override
        public void run()
        {
            try
            {
                gameSocket = new Socket("localhost", Main.GAME_CONNECTION);
                DataOutputStream toServer = new DataOutputStream(gameSocket.getOutputStream());
                DataInputStream fromServer = new DataInputStream(gameSocket.getInputStream());

                toServer.writeUTF(Main.setting.getUsername());
                toServer.writeBoolean(true);

                while(isInHome)
                {
                    boolean player2 = fromServer.readBoolean();
                    if(!player2) return;
                    isInHome = false;

                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../../ui/games/TicTacToeOnline.fxml"));
                    AnchorPane anchorPane = null;

                    try { anchorPane = fxmlLoader.load(); }
                    catch (IOException e) { e.printStackTrace(); }

                    TicTacToeOnlineController controller = fxmlLoader.getController();
                    controller.initFields(TicTacToeConstants.PLAYER2, gameSocket);
                    onlineSocket.close();
                    Main.loadFXML(anchorPane);
                }
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void gameComboBoxPressed()
    {
        Object[] info = getInfo();
        int friendNumber = (int) info[0];
        String[] friendList = (String[]) info[1];
        String[] lastSeenList = (String[]) info[2];
        boolean[] isOnlineList = (boolean[]) info[3];
        int[] avatarList = (int[]) info[4];

        for(int i = 0; i < friendNumber; i++)
        {
            if(isOnlineList[i])
            {
                gameComboBox.getItems().add(friendList[i]);
            }
        }
    }

    @FXML
    private void offlinePlayNowClicked()
    {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../../ui/games/TicTacToeOffline.fxml"));
        AnchorPane anchorPane = null;

        try { anchorPane = fxmlLoader.load(); }
        catch (IOException e) { e.printStackTrace(); }

        TicTacToeOfflineController controller = fxmlLoader.getController();

        if(xRadioButton.isSelected())
            controller.initFields('X');
        else
            controller.initFields('O');

        try
        {
            onlineSocket.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        Main.loadFXML(anchorPane);
    }

    @FXML
    private void onlinePlayNowClicked()
    {
        try
        {
            if(gameComboBox.getSelectionModel().getSelectedItem().isEmpty()) return;
            Socket socket = new Socket("localhost", Main.TIC_TAC_TOE_ONLINE);
            DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());

            toServer.writeUTF(Main.setting.getUsername());
            toServer.writeUTF(gameComboBox.getSelectionModel().getSelectedItem());

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../../ui/games/TicTacToeOnline.fxml"));
            AnchorPane anchorPane = null;

            try { anchorPane = fxmlLoader.load(); }
            catch (IOException e) { e.printStackTrace(); }

            TicTacToeOnlineController controller = fxmlLoader.getController();
            controller.initFields(TicTacToeConstants.PLAYER1, gameSocket);
            isInHome = false;
            onlineSocket.close();
            Main.loadFXML(anchorPane);
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    // chat
    class ChatHandler implements Runnable
    {
        @Override
        public void run()
        {
            try
            {
                Socket socket = new Socket("localhost", Main.UPDATE_CHAT_STATUS);
                DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());
                DataInputStream fromServer = new DataInputStream(socket.getInputStream());

                toServer.writeUTF(Main.setting.getUsername());
                toServer.writeBoolean(true);

                while(isInHome)
                {
                    String friendName = fromServer.readUTF();
                    String message = fromServer.readUTF();

                    updateChatMessagesVBox();
                }
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    public void updateChatNameVBox()
    {
        Object[] info = getInfo();
        int friendNumber = (int) info[0];
        String[] friendList = (String[]) info[1];
        String[] lastSeenList = (String[]) info[2];
        boolean[] isOnlineList = (boolean[]) info[3];
        int[] avatarList = (int[]) info[4];

        chatNameHBoxControllers.clear();
        Platform.runLater(() -> chatNameVBox.getChildren().clear());
        for(int i = 0; i < friendNumber; i++)
        {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../../ui/home/ChatNameHBox.fxml"));
            HBox hBox = null;

            try { hBox = fxmlLoader.load(); }
            catch (IOException e) { e.printStackTrace(); }

            ChatNameHBoxController controller = fxmlLoader.getController();
            chatNameHBoxControllers.put(friendList[i], controller);

            controller.initFields(friendList[i], lastSeenList[i], isOnlineList[i], avatarList[i], this);
            new SlideInUp(hBox).play();
            HBox finalHBox = hBox;
            Platform.runLater(() -> chatNameVBox.getChildren().add(finalHBox));
        }
    }

    public void updateChatMessagesVBox()
    {
        Platform.runLater(() -> chatMessageVBox.getChildren().clear());

        if(!chatSelected.isEmpty())
        {
            Message[] messages = loadMessages(chatSelected);

            for (Message message : messages)
            {
                addMessage(message.getSender(), message.getText());
            }
        }
        new Thread(() ->
        {
            try
            {
                Thread.sleep(200);
            } catch (InterruptedException exception)
            {
                exception.printStackTrace();
            }
            Platform.runLater(() -> chatScrollPane.setVvalue(1));
        }).start();
    }

    public Message[] loadMessages(String friendName)
    {
        Message[] messages = null;
        try
        {
            Socket socket = new Socket("localhost", Main.GET_MESSAGES);
            DataInputStream fromServer = new DataInputStream(socket.getInputStream());
            DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());

            toServer.writeUTF(Main.setting.getUsername());
            toServer.writeUTF(friendName);

            int numberOfMessages = fromServer.readInt();
            messages = new Message[numberOfMessages];

            for(int i = 0; i < numberOfMessages; i++)
                messages[i] = new Message(fromServer.readUTF(), fromServer.readUTF());

        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return messages;
    }

    public void addMessage(String username, String message)
    {
        FXMLLoader fxmlLoader;
        // if it is me
        if(username.equals(Main.setting.getUsername()))
            fxmlLoader = new FXMLLoader(getClass().getResource("../../ui/home/ChatRightMessageHBox.fxml"));
        else
            fxmlLoader = new FXMLLoader(getClass().getResource("../../ui/home/ChatLeftMessageHBox.fxml"));

        HBox hBox = null;

        try { hBox = fxmlLoader.load(); }
        catch (IOException e) { e.printStackTrace(); }

        ChatMessageHBox controller = fxmlLoader.getController();

        controller.initFields(username, message);
        HBox finalHBox = hBox;
        Platform.runLater(() -> chatMessageVBox.getChildren().add(finalHBox));
        new SlideInUp(finalHBox).play();
    }

    @FXML
    private void sendButtonPressed()
    {
        new Thread(() ->
        {
            try
            {
                Socket socket = new Socket("localhost", Main.SEND_MESSAGE);
                DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());

                toServer.writeUTF(Main.setting.getUsername());
                toServer.writeUTF(chatSelected);
                toServer.writeUTF(chatTA.getText());
                Platform.runLater(() -> chatTA.clear());
            } catch (IOException e)
            {
                e.printStackTrace();
            }
            try
            {
                Thread.sleep(500);
            } catch (InterruptedException exception)
            {
                exception.printStackTrace();
            }
            updateChatMessagesVBox();
        }).start();
    }

    @FXML
    void chatTAKeyPressed(KeyEvent event)
    {
        if(event.isControlDown() && event.getCode().getName().equals("Enter"))
        {
            sendButtonPressed();
        }
    }

    @FXML
    private void chatSelectionChanged()
    {
        updateChatNameVBox();
        chatSelected = "";
        information.setText("Platinum");
        updateChatMessagesVBox();
    }

    @FXML
    private void clearHistoryClicked()
    {
        new Thread(() ->
        {
            try
            {
                Socket socket = new Socket("localhost", Main.CLEAR_HISTORY);

                DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());

                toServer.writeUTF(Main.setting.getUsername());
                toServer.writeUTF(chatSelected);

                Thread.sleep(500);
                updateChatMessagesVBox();
            } catch (IOException | InterruptedException e)
            {
                e.printStackTrace();
            }
        }).start();
    }

    // friend
    @FXML
    private void addFriendClicked()
    {
        friendServerLabel.setVisible(true);
        friendServerSpinner.setVisible(true);
        new FadeIn(friendServerLabel).setSpeed(0.5).play();
        new FadeIn(friendServerSpinner).setSpeed(0.5).play();

        // connect to server for data validation
        new Thread(() ->
        {
            try
            {
                Socket socket = new Socket("localhost", Main.ADD_FRIEND);
                Thread.sleep(1000);
                Platform.runLater(() -> friendServerLabel.setText("Connected to server!"));
                Thread.sleep(500);

                DataInputStream fromServer = new DataInputStream(socket.getInputStream());
                DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());

                toServer.writeUTF(Main.setting.getUsername());
                toServer.writeUTF(addFriendTF.getText());
                boolean successful = fromServer.readBoolean();
                socket.close();

                if (successful)
                {
                    Platform.runLater(() -> friendServerLabel.setText("Done!"));
                    updateFriendVBox();
                    updateChatNameVBox();
                    Thread.sleep(1500);
                    disableServerComponents();
                } else
                {
                    // react to user for mismatch
                    Platform.runLater(() -> friendServerLabel.setText("something is wrong"));
                }
                Thread.sleep(1500);
                disableServerComponents();

            } catch (ConnectException connectException)
            {
                // react to user for connection problem
                Platform.runLater(() -> friendServerLabel.setText("couldn't connect to server!"));
                try
                {
                    Thread.sleep(1500);
                } catch (InterruptedException exception)
                {
                    exception.printStackTrace();
                }

                // reset the server label and spinner
                disableServerComponents();
            } catch (IOException | InterruptedException exception)
            {
                exception.printStackTrace();
            }
        }).start();
    }

    @FXML
    private void refreshClicked()
    {
        updateFriendVBox();
    }

    @FXML
    private void friendsClicked()
    {
        updateFriendVBox();
    }

    private void disableServerComponents()
    {
        Platform.runLater(() ->
        {
            friendServerLabel.setVisible(false);
            friendServerSpinner.setVisible(false);
            friendServerLabel.setText("connecting to server...");
        });
    }

    public void updateFriendVBox()
    {
        Object[] info = getInfo();
        int friendNumber = (int) info[0];
        String[] friendList = (String[]) info[1];
        String[] lastSeenList = (String[]) info[2];
        boolean[] isOnlineList = (boolean[]) info[3];
        int[] avatarList = (int[]) info[4];


        friendHBoxControllers.clear();
        Platform.runLater(() -> friendsVBox.getChildren().clear());
        for(int i = 0; i < friendNumber; i++)
        {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../../ui/home/FriendHBox.fxml"));
            HBox hBox = null;

            try { hBox = fxmlLoader.load(); }
            catch (IOException e) { e.printStackTrace(); }

            FriendHBoxController controller = fxmlLoader.getController();
            friendHBoxControllers.put(friendList[i], controller);

            controller.initFields(friendList[i], lastSeenList[i], isOnlineList[i], avatarList[i], this);
            new SlideInUp(hBox).play();
            HBox finalHBox = hBox;
            Platform.runLater(() -> friendsVBox.getChildren().add(finalHBox));
        }
    }

    private void updateAvatarOnServer()
    {
        try
        {
            Socket socket = new Socket("localhost", Main.UPDATE_AVATAR_NUMBER);
            DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());

            toServer.writeUTF(Main.setting.getUsername());
            toServer.writeInt(Main.setting.getAvatarNumber());
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }


    // setting
    class SettingHandler implements Runnable
    {
        @Override
        public void run()
        {
            bindComponents();
            loadAvatars();
            boldSelectedAvatar();
        }

        private void bindComponents()
        {
            Main.setting.volumeProperty().bind(volumeSlider.valueProperty().divide(100));
            Main.setting.soundProperty().bind(musicToggle.selectedProperty());
            Main.setting.sfxProperty().bind(sfxToggle.selectedProperty());
        }

        private void loadAvatars()
        {
            for(int i = 0; i < 12; i++)
            {
                circles[i].setFill(new ImagePattern(new Image("main/resources/pictures/user" + (i + 1) + ".png")));
            }
        }

        private void boldSelectedAvatar()
        {
            int avatarNumber = Main.setting.getAvatarNumber();
            circles[avatarNumber - 1].setStyle("-fx-stroke: red");
        }
    }

    @FXML
    private void avatarCircleClicked(MouseEvent event)
    {
        Circle avatar = (Circle) event.getSource();

        circles[Main.setting.getAvatarNumber() - 1].setStyle("-fx-stroke: white");
        avatar.setStyle("-fx-stroke: red");
        new ZoomIn(avatar).play();
        Main.mediaPlayer.playSelect();

        int avatarNumber = 1;
        for(int i = 0; i < 12; i++)
        {
            if(circles[i] == avatar)
                avatarNumber = i + 1;
        }
        Main.setting.setAvatarNumber(avatarNumber);
        updateAvatarOnServer();
        avatarCircle.setFill(new ImagePattern(new Image("main/resources/pictures/user" + avatarNumber + ".png")));
    }

    @FXML
    private void togglePressed()
    {
        Main.mediaPlayer.playToggleClick();
    }

    @FXML
    private void logOutPressed()
    {
        new RubberBand(logOutButton).play();
        isInHome = false;
        Main.mediaPlayer.playButtonClick();

        Main.makeOffline(Main.setting.getUsername());
        Main.mediaPlayer.stop();
        Main.loadFXML("ui/login/Login.fxml");
    }

    @FXML
    private void exitButtonPressed()
    {
        new RubberBand(exitButton).play();
        isInHome = false;
        Main.mediaPlayer.playButtonClick();

        // something to be added here

        Main.closeProgram();
    }
}

class Message
{
    private final String sender;
    private final String text;

    public Message(String sender, String text)
    {
        this.sender = sender;
        this.text = text;
    }

    public String getSender() { return sender; }
    public String getText() { return text; }
}