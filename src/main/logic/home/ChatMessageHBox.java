package main.logic.home;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import main.Main;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ChatMessageHBox
{
    @FXML private Circle avatar;
    @FXML private Label label;

    public void initFields(String username, String message)
    {
        int avatarNumber = 1;
        try
        {
            Socket socket = new Socket("localhost", Main.GET_AVATAR_NUMBER);

            DataInputStream fromServer = new DataInputStream(socket.getInputStream());
            DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());

            toServer.writeUTF(username);
            avatarNumber = fromServer.readInt();
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        // send the message to server
        avatar.setFill(new ImagePattern(new Image("main/resources/pictures/user" + avatarNumber + ".png")));
        label.setText(message);
    }
}
