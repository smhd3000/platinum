package main.logic.home;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;

public class ChatNameHBoxController
{
    @FXML private Circle avatarCircle;
    @FXML private Circle isOnlineCircle;
    @FXML private Label usernameLabel;
    @FXML private Label lastSeenLabel;

    private HomeController homeController;

    public void initFields(String username, String lastSeen, boolean isOnline, int avatarNumber, HomeController homeController)
    {
        usernameLabel.setText(username);
        if(isOnline)
        {
            makeOnline();
        } else
        {
            makeOffline(lastSeen);
        }
        avatarCircle.setFill(new ImagePattern(new Image("main/resources/pictures/user" + avatarNumber + ".png")));
        this.homeController = homeController;
    }

    @FXML
    private void chatNameHBoxClicked()
    {
        homeController.information.setText(usernameLabel.getText());
        homeController.chatSelected = usernameLabel.getText();
        homeController.updateChatMessagesVBox();
    }

    public void makeOnline()
    {
        Platform.runLater(() ->
        {
            lastSeenLabel.setText("online");
            isOnlineCircle.setStyle("-fx-fill: green");
        });
    }

    public void makeOffline(String lastSeen)
    {
        Platform.runLater(() ->
        {
            lastSeenLabel.setText(lastSeen);
            isOnlineCircle.setStyle("-fx-fill: red");
        });
    }
}
