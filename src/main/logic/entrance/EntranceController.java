package main.logic.entrance;

import animatefx.animation.FadeInLeftBig;
import animatefx.animation.FadeInRightBig;
import animatefx.animation.Shake;
import animatefx.animation.SlideInDown;
import com.jfoenix.controls.JFXSpinner;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import main.Main;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class EntranceController implements Initializable
{
    @FXML private ImageView logo;
    @FXML private Label title;
    @FXML private Label subTitle;
    @FXML private JFXSpinner spinner;

    private Parent nextPane;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        new Thread(() ->
        {
            try
            {
                nextPane = FXMLLoader.load(Main.class.getResource("ui/login/Login.fxml"));
            } catch (IOException exception)
            {
                exception.printStackTrace();
            }
        }).start();

        new SlideInDown(logo).setSpeed(0.5).play();
        new FadeInLeftBig(title).setSpeed(0.5).play();
        new FadeInRightBig(subTitle).setSpeed(0.5).play();
        Shake spinnerAnimation = new Shake(spinner);
        spinnerAnimation.setSpeed(0.5);
        spinnerAnimation.play();

        spinnerAnimation.setOnFinished(event1 ->
                new Thread(() ->
                {
                    try
                    {
                        Thread.sleep(5000);
                        Main.loadFXML(nextPane);
                    } catch (InterruptedException exception)
                    {
                        exception.printStackTrace();
                    }
                }).start());
    }
}
