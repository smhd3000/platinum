package main;

import animatefx.animation.FadeIn;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;

public class Main extends Application
{
    public static Scene scene;
    public static Setting setting;
    public static SoundPlayer mediaPlayer;

    public static final int SIGN_UP_PORT = 8000;
    public static final int LOGIN_PORT = 8001;
    public static final int RESTORE_PORT = 8002;
    public static final int ONLINE_PORT = 8003;
    public static final int GET_AVATAR_NUMBER = 8004;
    public static final int UPDATE_AVATAR_NUMBER = 8005;
    public static final int ADD_FRIEND = 8006;
    public static final int DELETE_FRIEND = 8007;
    public static final int GET_FRIEND_INFO = 8008;
    public static final int GAME_STATISTICS = 8009;
    public static final int GET_MESSAGES = 8010;
    public static final int UPDATE_CHAT_STATUS = 8011;
    public static final int SEND_MESSAGE = 8012;
    public static final int CLEAR_HISTORY = 8013;
    public static final int UPDATE_GAME_STATISTICS = 8014;
    public static final int TIC_TAC_TOE_ONLINE = 8015;
    public static final int GAME_CONNECTION = 8016;

    public static void main(String[] args) { launch(args); }

    @Override
    public void start(Stage primaryStage) throws IOException
    {
        mediaPlayer = new SoundPlayer();

        // configuring settings
        setting = new Setting();
        setting.soundProperty().addListener(((observable, oldValue, newValue) ->
        {
            if(newValue) mediaPlayer.unMute();
            else mediaPlayer.mute();
        }));

        setting.volumeProperty().addListener(((observable, oldValue, newValue) -> mediaPlayer.setVolume(newValue.doubleValue())));
        setting.setSfx(true);
        setting.setVolume(0.8);

        Parent entrance = FXMLLoader.load(getClass().getResource("ui/entrance/Entrance.fxml"));
        Scene scene = new Scene(entrance, 600, 600);
        Main.scene = scene;

        primaryStage.getIcons().add(new Image("file:src/main/resources/pictures/icon.jpg"));
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Platinum");
        primaryStage.show();
        primaryStage.setOnCloseRequest(event -> closeProgram());
    }

    public static void loadFXML(String url)
    {
        new Thread(() ->
        {
            try
            {
                Parent pane = FXMLLoader.load(Main.class.getResource(url));
                loadFXML(pane);
            } catch (IOException exception)
            {
                exception.printStackTrace();
            }
        }).start();
    }

    public static void loadFXML(Parent pane)
    {
        new Thread(() ->
        {
            Platform.runLater(() -> scene.setRoot(pane));
            new FadeIn(pane).setSpeed(0.7).play();
        }).start();
    }

    public static void makeOffline(String username)
    {
        // make the user offline and update lastSeen
        if(!username.equals("Player"))
        {
            try
            {
                Socket socket = new Socket("localhost", ONLINE_PORT);
                DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());

                toServer.writeUTF(username);
                toServer.writeBoolean(false);

                Socket socket1 = new Socket("localhost", UPDATE_CHAT_STATUS);
                DataOutputStream toServer1 = new DataOutputStream(socket1.getOutputStream());

                toServer1.writeUTF(setting.getUsername());
                toServer1.writeBoolean(false);

                Socket socket2 = new Socket("localhost", GAME_CONNECTION);
                DataOutputStream toServer2 = new DataOutputStream(socket2.getOutputStream());

                toServer2.writeUTF(setting.getUsername());
                toServer2.writeBoolean(false);
            }
            catch (ConnectException ignored) {}
            catch (IOException exception)
            {
                exception.printStackTrace();
            }
        }
    }

    public static void getAvatarNumber()
    {
        int avatarNumber = 1;
        try
        {
            Socket socket = new Socket("localhost", Main.GET_AVATAR_NUMBER);

            DataInputStream fromServer = new DataInputStream(socket.getInputStream());
            DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());

            toServer.writeUTF(Main.setting.getUsername());
            avatarNumber = fromServer.readInt();
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        setting.setAvatarNumber(avatarNumber);
    }

    public static void closeProgram()
    {
        makeOffline(setting.getUsername());
        System.exit(0);
    }
}
